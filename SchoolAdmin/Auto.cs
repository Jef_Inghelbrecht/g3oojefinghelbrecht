﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SchoolAdmin
{
    class Auto
    {

        public string Merk { get; set; }
        public int AantalWielen { get; set; }

        public Auto()
        {

        }

        public Auto(string merk, int aantalWielen)
        {
            this.Merk = merk;
            this.AantalWielen = aantalWielen;
        }

        public Auto(string merk)
        {
            this.Merk = merk;
        }

    }
}
