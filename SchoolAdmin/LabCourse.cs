﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SchoolAdmin
{
    class LabCourse : Course
    {
        public byte StudyPoints { get; set; }
        public string Materials { get; set; }

        public LabCourse(string title) : base(title)
        {
        }
        // methode moet nog aangepast worden
        public override uint CalculateWorkload()
        {
            return this.StudyPoints * (uint) 2;
        }
    }
}
