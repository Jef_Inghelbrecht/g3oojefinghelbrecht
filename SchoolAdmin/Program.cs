﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace SchoolAdmin
{
    class Program
    {
        static void Main(string[] args)
        {
            bool done = false;
            while (!done)
            {
                Console.WriteLine("Welkom bij Schoolbeheer!\n Maak een keuze uit de volgende lijst: ");
                Console.WriteLine("************************************************");
                Console.WriteLine("1. De School klasse testen");
                Console.WriteLine("2. De Student klasse testen");
                Console.WriteLine("3. De Lecturer klasse testen");
                Console.WriteLine("4. Een lijst met Person instanties maken");
                Console.WriteLine("5. De Administrative klasse gebruiken");
                Console.WriteLine("6. De associatie Lecturer en Course klasse");
                Console.WriteLine("7. De verschillende personen met naamkaartje weergeven");
                Console.WriteLine("8. Auto's");
                Console.WriteLine("9. Polymorfisme");

                Console.WriteLine("0. Stoppen");
                int choise = int.Parse(Console.ReadLine());
                switch (choise)
                {
                    case 1:
                        TestSchool();
                        break;
                    case 2:
                        TestStudent();
                        break;
                    case 3:
                        TestLecturer();
                        break;
                    case 4:
                        MakePerson();
                        break;
                    case 5:
                        TestAdministrativeStaff();
                        break;
                    case 6:
                        TestLectureCourse();
                        break;
                    case 7:
                        PersonListMetNaamkaartje();
                        break;
                    case 8:
                        TestAuto();
                        break;
                    case 9:
                        Polymorphisme();
                        break;
                    case 0:
                        done = true;
                        break;
                }
            }
        }

        static void TestSchool()
        {
            // Labo 8: 2 nieuwe scholen aanmaken, in list steken en in de console tonen
            School school1 = new School("GO! BS de Spits", "Thonetlaan 106", "2050", "Antwerpen", 1);
            School school2 = new School("GO! Koninklijk Atheneum", "Fr. Craeybeckxlaan 22", "2100", "Deurne", 2);
            //School.List = new List<School>
            //{
            //    school1,
            //    school2
            //};
            School.List = new List<School>();
            School.List.Add(school1);
            School.List.Add(school2);

            List<School> schoolList = new List<School>();
            schoolList.Add(school1);
            schoolList.Add(school2);

            School.List = schoolList;

            Console.WriteLine(school1.ShowOne());   // Slechts één school tonen
            Console.WriteLine(School.ShowAll()); // alle scholen tonen
        }

        static List<Student> TestStudent()
        {
            // Labo 8: 4 nieuwe studenten aanmaken en in list steken
            Student student1 = new Student("Mohamed", "El Farisi", new DateTime(1987, 12, 06), 1, 1, "0489000000");
            Student student2 = new Student("Sarah", "Jansens", new DateTime(1991, 10, 21), 2, 3, "0489000000");
            Student student3 = new Student("Bart", "Jansens", new DateTime(1991, 10, 21), 2, 3, "0489000000");
            Student student4 = new Student("Farah", "El Farisi", new DateTime(1987, 12, 06), 1, 4, "0489000000");
            Student.List = new List<Student>
    {
        student1,
        student2,
        student3,
        student4
    };
            Console.WriteLine(Student.ShowAll());   // Alle studenten tonen
            Console.WriteLine(student1.ShowOne());  // Slechts één student tonen
            return Student.List;
        }

        static List<Lecturer> TestLecturer()
        {
            // Labo 9 en 10: 2 nieuwe lectoren aanmaken en in een list steken
            Lecturer lecturer1 = new Lecturer("Adem", "Kaya", new DateTime(1976, 12, 01), 1, 1, "0489000000");
            Lecturer lecturer2 = new Lecturer("Anne", "Wouters", new DateTime(1968, 04, 03), 2, 2, "0489000000");
            Lecturer.List = new List<Lecturer>
    {
        lecturer1,
        lecturer2
    };
            Console.WriteLine(Lecturer.ShowAll());
            Console.WriteLine(lecturer1.ShowOne());
            return Lecturer.List;
        }

        static List<Person> MakePerson()
        {
            // Labo 9 en 10: Een lijst van Person maken met zowel studenten als lectoren in
            // maak de lijst met studenten
            List<Student> studentList = TestStudent();
            // maak de lijst met lectoren
            List<Lecturer> lecturerList = TestLecturer();
            var persons = new List<Person>();
            foreach (Student student in studentList)
            {
                persons.Add(student);
            }

            foreach (Lecturer lecturer in lecturerList)
            {
                persons.Add(lecturer);
            }
            return persons;
        }

        static void TestAdministrativeStaff()
        {
            // Labo 9 en 10: Object maken van AdministrativeStaff en toevoegen aan de lijst van personen
            var administrativeStaff1 = new AdministrativeStaff("Raul", "Jacob", new DateTime(1985, 11, 01), 1, 1, "0489000000");
            List<Person> persons = MakePerson();
            persons.Add(administrativeStaff1);
            Console.WriteLine(administrativeStaff1.ShowOne());
            foreach (var person in persons)
            {
                Console.WriteLine($"{person.FirstName} {person.LastName}");
            }
            // Labo 9 en 10: De nameTagText tonen voor de 3 soorten personen
            // Zorg ervoor dat voor administratief personeel de tekst (ADMINISTRATIE) 
            // verschijnt op het naamkaartje, vlak voor de volledige naam
            Console.WriteLine(administrativeStaff1.GetNameTagText());
        }

        static void TestLectureCourse()
        {
            // Labo 11: Associëren van lectoren met hun cursussen en aanvullen ShowTaughtCourses()
            // Aanmaken van cursus OOP en associëren met lecturer1
            // Aanmaken seminarie Docker en associëren met lecturer1
            Course theory1 = new TheoryCourse("Theorie OO Programmeren", 3);
            Course seminar = new Seminar("Docker");
            Course lab1 = new LabCourse("labo Webtechnolgy");
            Lecturer lecturer1 = new Lecturer("Adem", "Kaya", new DateTime(1976, 12, 01), 1, 1, "0489000000");

            lecturer1.Courses.Add(theory1);
            lecturer1.Courses.Add(seminar);
            lecturer1.Courses.Add(lab1);
            Console.WriteLine($"{lecturer1.ShowTaughtCourses()}");
        }

        static void PersonListMetNaamkaartje()
        {
            // Labo 11: Voeg een protected property ContactNumber(van type string) toe aan Person en zorg 
            // dat deconstructoren dit ook aannemen; zorg dat dit getoond wordt op de naamkaartjes
            // van personeel(Lecturer en AdministrativeStaff) maar niet van studenten.
            Lecturer lecturer1 = new Lecturer("Adem", "Kaya", new DateTime(1976, 12, 01), 1, 1, "0489000000");
            Student student1 = new Student("Mohamed", "El Farisi", new DateTime(1987, 12, 06), 1, 1, "0489000000");
            var administrativeStaff1 = new AdministrativeStaff("Raul", "Jacob", new DateTime(1985, 11, 01), 1, 1, "0489000000");
            Console.WriteLine(student1.GetNameTagText());
            Console.WriteLine(lecturer1.GetNameTagText());
            Console.WriteLine(administrativeStaff1.GetNameTagText());
            Console.ReadLine();
        }

        static void TestAuto()
        {
            Auto renault = new Auto("Clio", 5);
            Auto big = new Auto("Chevrolet", 8);

            List<Auto> autoList = new List<Auto>();
            autoList.Add(renault);
            autoList.Add(big);

            foreach (Auto eneAuto in autoList)
            {
                Console.WriteLine($"{eneAuto.Merk} met {eneAuto.AantalWielen} wielen.");
            }

            Auto small = new Auto();
            small.Merk = "Fiat";
            small.AantalWielen = 2;

        }

        static void Polymorphisme()
        {
            // een lijst maken met alle personen in de school
            //
            Student student1 = new Student("Mohamed", "El Farisi", new DateTime(1987, 12, 06), 1, 1, "0489000000");
            Student student2 = new Student("Sarah", "Jansens", new DateTime(1991, 10, 21), 2, 3, "0489000000");

            Lecturer lecturer1 = new Lecturer("Adem", "Kaya", new DateTime(1976, 12, 01), 1, 1, "0489000000");
            Lecturer lecturer2 = new Lecturer("Anne", "Wouters", new DateTime(1968, 04, 03), 2, 2, "0489000000");
            var administrativeStaff1 = new AdministrativeStaff("Raul", "Jacob", new DateTime(1985, 11, 01), 1, 1, "0489000000");
            // maak een lijst van al die personen

            List<Person> listPerson = new List<Person>();
            listPerson.Add(student1);
            listPerson.Add(student2);
            listPerson.Add(lecturer1);
            listPerson.Add(lecturer2);
            listPerson.Add(administrativeStaff1);

            Console.WriteLine("Alle personen in de school:\n");
            foreach (Person onePerson in listPerson)
            {
                Console.WriteLine($"{onePerson.FirstName} {onePerson.GetNameTagText()}");
            }

        }
    }
}
