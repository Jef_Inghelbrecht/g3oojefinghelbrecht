﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class Patient
    {
        private string _name;

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public enum gender {man,vrouw}
        private gender gender1;

        public gender Gender
        {
            get { return gender1; }
            set { gender1 = value; }
        }

        private string lifestyles;

        public string LifeStyles
        {
            get { return lifestyles; }
            set { lifestyles = value; }
        }

        private int days;

        public int Days
        {
            get { return days; }
            set { days = value; }
        }

        private int months;

        public int Months
        {
            get { return months; }
            set { months = value; }
        }

        private int years;

        public int Years
        {
            get { return years; }
            set { years = value; }
        }
    }
}
