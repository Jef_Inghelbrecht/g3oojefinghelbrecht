﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP.Geometry
{
    class ShapesBuilder
    {
        private ConsoleColor color;
        public ConsoleColor Color
        {
            get { return color; }
            set
            {
                color = value;
                Console.ForegroundColor = color;
            }
        }

        private char symbol;
        public char Symbol
        {
            get { return symbol; }
            set { 
                if (symbol == null)
                {
                    symbol = '_';
                }
                else
                {
                    symbol = value;
                }
            }
        }

        public string Line(int length)
        {
            return new string(symbol, length);
        }

        public string Line(string length)
        {
            return new string(symbol, Convert.ToInt32(length));
        }

        public string Line(int length, char alternateSymbol)
        {
            return new string(alternateSymbol, length);
        }

        public string Rectangle(int height, int width)
        {
            return Rectangle(height, width, symbol);
        }

        public string Rectangle(int height, int width, char alternateSymbol)
        {
            string output = "";
            for (int i = 0; i < height; i++)
            {
                output += Line(width,alternateSymbol);
                if (i < height - 1)
                {
                    output += "\n";
                }
            }
            return output;
        }

        public string Triangle(int height)
        {
            return Triangle(height, symbol);
        }

        public string Triangle(int height, char alternateSymbol)
        {
            string output = "";
            for (int i = 0; i < height; i++)
            {
                output += Line(i + 1,alternateSymbol);
                if (i < height - 1)
                {
                    output += "\n";
                }
            }
            return output;
        }
    }
}
