﻿using OOP.Geometry;
using System;

namespace OOP
{
    class Program
    {
        static void Main(string[] args)
        {
            int choice;
            while(true)
            {
                Console.ResetColor();
                Console.WriteLine("Wat wil je doen?");
                Console.WriteLine("1. Vormen tekenen (h8-vormen)");
                Console.WriteLine("2. Auto's laten rijden (h8-autos)");
                Console.WriteLine("3. Patienten (h8-patienten)");
                Console.Write("\nKeuze: ");
                choice = int.Parse(Console.ReadLine());
                Console.Write("\n\n");
                switch (choice)
                {
                    case 1:
                        ShapesBuilder builder = new ShapesBuilder();
                        //ander object dan builder, verder niet gebruikt
                        ShapesBuilder anotherbuilder;
                        anotherbuilder = new ShapesBuilder();

                        builder.Color = ConsoleColor.Green;
                        builder.Symbol = '*';
                        string line = builder.Line(10);
                        Console.WriteLine(line);
                        builder.Color = ConsoleColor.Red;
                        string rectangle = builder.Rectangle(10, 5);
                        Console.WriteLine(rectangle);
                        builder.Color = ConsoleColor.White;
                        string triangle = builder.Triangle(10);
                        Console.WriteLine(triangle);
                        string alternateTriangle = builder.Triangle(10,'a');
                        Console.WriteLine(alternateTriangle);
                        string thirdTriangle = builder.Triangle(5);
                        Console.WriteLine(thirdTriangle);
                        Console.ReadKey();
                        Console.Clear();
                        break;
                    case 2:
                        Car car1 = new Car();
                        for(int i = 1; i <= 5; i++)
                        {
                            car1.Gas();
                        }
                        for (int i = 1; i <= 3; i++)
                        {
                            car1.Brake();
                        }
                        Car car2 = new Car();
                        for (int i = 1; i <= 10; i++)
                        {
                            car2.Gas();
                        }
                        car2.Brake();
                        Console.WriteLine($"Auto 1: {car1.Speed:f2}km/u, afgelegde weg {car1.Odometer:f2}km");
                        Console.WriteLine($"Auto 2: {car2.Speed:f2}km/u, afgelegde weg {car2.Odometer:f2}km");
                        Console.ReadKey();
                        Console.Clear();
                        break;
                    case 3:
                        Patient[] patienten = new Patient[2];

                        patienten[0] = new Patient();
                        patienten[0].Name = "Man";
                        patienten[0].Gender = Patient.gender.man;
                        patienten[0].LifeStyles = "iets";
                        patienten[0].Days = 1;
                        patienten[0].Months = 1;
                        patienten[0].Years = 2020;

                        patienten[1] = new Patient();
                        patienten[1].Name = "Vrouw";
                        patienten[1].Gender = Patient.gender.vrouw;

                        Console.WriteLine($"De naam van patient 0 is {patienten[0].Name}\nGeslacht is {patienten[0].Gender}");
                        //...
                        Console.ReadKey();
                        Console.Clear();
                        break;
                    default:
                        Console.WriteLine("Ongeldige keuze!");
                        break;
                }
            }
        }
    }
}
