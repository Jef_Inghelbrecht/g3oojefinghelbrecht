﻿using System;
using System.Collections.Generic;

namespace SchoolAdmin
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Schoolbeheer");
            var school1 = new School();
            school1.Name = "GO! BS de Spits";
            school1.Street = "Thonetlaan 106";
            school1.PostalCode = "2050";
            school1.City = "Antwerpen";
            school1.Id = 1;
            var school2 = new School();
            school2.Name = "GO! Koninklijk Atheneum Deurne";
            school2.Street = "Fr. Craeybeckxlaan 22";
            school2.PostalCode = "2100";
            school2.City = "Deurne";
            school2.Id = 2;
            School.List = new List<School>();
            School.List.Add(school1);
            School.List.Add(school2);
            Console.WriteLine(School.ShowAll());
            var student1 = new Student();
            student1.FirstName = "Mohamed";
            student1.LastName = "El Farisi";
            student1.BirthDay = new DateTime(1987, 12, 06);
            student1.Id = 1;
            student1.SchoolId = 1;
            var student2 = new Student();
            student2.FirstName = "Sarah";
            student2.LastName = "Jansens";
            student2.BirthDay = new DateTime(1991, 10, 21);
            student2.Id = 2;
            student2.SchoolId = 2;
            var student3 = new Student();
            student2.FirstName = "Bart";
            student2.LastName = "Jansens";
            student2.BirthDay = new DateTime(1991, 10, 21);
            student2.Id = 3;
            student2.SchoolId = 2;
            var student4 = new Student();
            student2.FirstName = "Farah";
            student2.LastName = "El Farisi";
            student2.BirthDay = new DateTime(1987, 12, 06);
            student2.Id = 4;
            student2.SchoolId = 1;
            Student.List = new List<Student>();
            Student.List.Add(student1);
            Student.List.Add(student2);
            Student.List.Add(student3);
            Student.List.Add(student4);
            Console.WriteLine(Student.ShowAll());

            var lecturer1 = new Lecturer();
            lecturer1.FirstName = "Adem";
            lecturer1.LastName = "Kaya";
            lecturer1.BirthDay = new DateTime(1976, 12, 01);
            lecturer1.Id = 1;
            lecturer1.SchoolId = 1;
            Console.WriteLine(lecturer1.ShowTaughtCourses());
            var lecturer2 = new Lecturer();
            lecturer2.FirstName = "Anne";
            lecturer2.LastName = "Wouters";
            lecturer2.BirthDay = new DateTime(1968, 04, 03);
            lecturer2.Id = 2;
            lecturer2.SchoolId = 2;
            Lecturer.List = new List<Lecturer>();
            Lecturer.List.Add(lecturer1);
            Lecturer.List.Add(lecturer2);
            Console.WriteLine(Lecturer.ShowAll());
            Console.WriteLine(lecturer1.ShowOne());

            AdministrativeStaff.List = new List<AdministrativeStaff>();
            var administrativeStaff1 = new AdministrativeStaff();
            administrativeStaff1.FirstName = "Raul";
            administrativeStaff1.LastName = "Jacob";
            administrativeStaff1.BirthDay = new DateTime(1985, 11, 1);
            administrativeStaff1.Id = 1;
            administrativeStaff1.SchoolId = 1;
            AdministrativeStaff.List.Add(administrativeStaff1);
            Console.WriteLine(AdministrativeStaff.ShowAll());
            Console.WriteLine(administrativeStaff1.ShowOne());

            Console.WriteLine("Lijst met personen:");
            var persons = new List<Person>();
            persons.Add(lecturer1);
            persons.Add(student1);
            persons.Add(administrativeStaff1);
            foreach(var person in persons)
            {
                Console.WriteLine(person.GetNameTagText());
            }
            Console.ReadLine();
        }
    }
}
