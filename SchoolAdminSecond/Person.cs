﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SchoolAdmin
{
    abstract class Person
    {

        public string FirstName { get; set; }

        private string lastName;

        public string LastName
        {
            get { return lastName; }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    lastName = value;
                }
            }
        }

        public DateTime BirthDay { get; set; }

        public int Id { get; set; }

        public int SchoolId { get; set; }

        public abstract string ShowOne();

        public virtual string GetNameTagText()
        {
            return $"{this.FirstName} {this.LastName}";
        }
    }
}
