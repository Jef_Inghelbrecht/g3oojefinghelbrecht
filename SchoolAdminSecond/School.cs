﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SchoolAdmin
{
    class School
    {
        private string name;

        public string Name
        {
            get { return name; }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    this.name = value;
                }
            }
        }

        public string Street { get; set; }

        public string PostalCode { get; set; }

        public string City { get; set; }

        public int Id { get; set; }

        public static List<School> List;

        public static string ShowAll()
        {
            string text = "Lijst van scholen:\n";
            foreach(var school in School.List)
            {
                text += $"{school.Name}, {school.Street}, {school.City}, {school.Id}\n";
            }
            return text;
        }

        public string ShowOne()
        {
            return $"Gegevens van de school: {this.Name}, {this.Street}, {this.City}, {this.Id}";
        }

    }
}
