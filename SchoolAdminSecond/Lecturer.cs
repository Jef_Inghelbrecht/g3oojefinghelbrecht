﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SchoolAdmin
{
    class Lecturer : Person
    {

        public static List<Lecturer> List { get; set; }

        public static string ShowAll()
        {
            string text = "Lijst van lectoren:\n";
            foreach (var lecturer in Lecturer.List)
            {
                text += $"{lecturer.FirstName}, {lecturer.LastName}, {lecturer.BirthDay}, {lecturer.Id}, {lecturer.SchoolId}";
            }
            return text;
        }

        public override string ShowOne()
        {
            return $"Gegevens van de lector: {this.FirstName}, {this.LastName}, {this.BirthDay}, {this.Id}, {this.SchoolId}";
        }

        public string ShowTaughtCourses()
        {
            return $"Vakken van deze lector: ???";
        }
        public override string GetNameTagText()
        {
            return $"(LECTOR) {this.FirstName} {this.LastName}";
        }
    }
}
