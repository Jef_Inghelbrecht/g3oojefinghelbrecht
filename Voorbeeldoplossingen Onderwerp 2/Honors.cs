﻿namespace OOP
{
    public enum Honors
    {
        NietGeslaagd,
        Voldoende,
        Onderscheiding,
        GroteOnderscheiding,
        GrootsteOnderscheiding
    }
}
