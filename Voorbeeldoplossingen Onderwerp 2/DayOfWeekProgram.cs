﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace OOP
{
    public class DayOfWeekProgram
    {
        public static void Main()
        {
            Console.WriteLine("Welke dag?");
            int day = int.Parse(Console.ReadLine());
            Console.WriteLine("Welke maand?");
            int month = int.Parse(Console.ReadLine());
            Console.WriteLine("Welk jaar");
            int year = int.Parse(Console.ReadLine());
            DateTime dateTime = new DateTime(year, month, day);
            CultureInfo belgianCI = new CultureInfo("nl-BE");
            Console.WriteLine($"{dateTime.ToString("dd MMMM yyyy", belgianCI)} is een {dateTime.ToString("dddd", belgianCI)}");
        }
    }
}
