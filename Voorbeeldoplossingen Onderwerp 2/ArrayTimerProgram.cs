﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    public class ArrayTimerProgram
    {
        public static void Main()
        {
            DateTime start = DateTime.Now;
            int[] myArray = new int[1000000];
            for(int i = 0; i < myArray.Length; i++)
            {
                myArray[i] = i + 1;
            }
            DateTime end = DateTime.Now;
            TimeSpan timeSpan = end - start;
            Console.WriteLine($"Het duurt {timeSpan.Milliseconds} milliseconden om een array van een miljoen elementen aan te maken en op te vullen met opeenvolgende waarden.");
        }
    }
}
