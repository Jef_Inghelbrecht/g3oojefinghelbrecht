﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    public class Ticks2000Program
    {
        public static void Main()
        {
            DateTime year2000 = new DateTime(2000, 1, 1);
            DateTime now = DateTime.Now;
            // optie 1
            TimeSpan difference = now - year2000;
            Console.WriteLine($"Sinds 1 januari 2000 zijn er {difference.Ticks} ticks voorbijgegaan.");
            Console.WriteLine($"Sinds 1 januari 2000 zijn er {now.Ticks - year2000.Ticks} ticks voorbijgegaan.");
        }
    }
}
