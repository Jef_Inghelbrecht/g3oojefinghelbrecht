﻿using System;

namespace OOP
{
    public class Program
    {
        public static void Main(string[] args)
        {
            int choice;
            while (true)
            {
                Console.WriteLine("Welke oefening wil je uitvoeren?");
                Console.WriteLine("1. Welke dag van de week? (H8-dag-van-de-week)");
                Console.WriteLine("2. Aantal ticks sinds 2000? (H8-ticks-sinds-2000)");
                Console.WriteLine("3. Aantal schrikkeljaren tussen 1800 en 2020? (H8-schrikkelteller)");
                Console.WriteLine("4. Timen code? (H8-simpele-timing)");
                Console.WriteLine("5. Printen resultaten V1 (H8-RapportModule-V1)");
                Console.WriteLine("6. Printen resultaten V2 (H8-RapportModule-V2)");
                Console.WriteLine("7. Getallencombinaties maken (H8-Getallencombinatie)");
                choice = int.Parse(Console.ReadLine());
                switch (choice)
                {
                    case 1:
                        DayOfWeekProgram.Main();
                        break;
                    case 2:
                        Ticks2000Program.Main();
                        break;
                    case 3:
                        LeapYearProgram.Main();
                        break;
                    case 4:
                        ArrayTimerProgram.Main();
                        break;
                    case 5:
                        ResultV1.Main();
                        break;
                    case 6:
                        ResultV2.Main();
                        break;
                    case 7:
                        NumberCombination.Main();
                        break;
                    default:
                        Console.WriteLine("Ongeldige keuze!");
                        break;
                }
            }
        }
    }
}
