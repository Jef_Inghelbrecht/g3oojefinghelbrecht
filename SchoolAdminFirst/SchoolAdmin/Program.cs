﻿using System;
using System.Collections.Generic;

namespace SchoolAdmin
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Schoolbeheer");
            School school1 = new School();
            school1.Name = "GO! BS de Spits";
            school1.Street = "Thonetlaan 106";
            school1.PostalCode = "2050";
            school1.City = "Antwerpen";
            school1.Id = 1;

            School school2 = new School();
            school2.Name = "GO! Koninklijk Atheneum Deurne";
            school2.Street = "Fr. Craeybeckxlaan 22";
            school2.PostalCode = "2100";
            school2.City = "Deurne";
            school2.Id = 2;

            List<School> schoolList = new List<School>();


            schoolList.Add(school1);
            schoolList.Add(school2);

            School.List = schoolList;
            Console.WriteLine(School.ShowAll());
            Console.ReadLine();

        }
    }
}
