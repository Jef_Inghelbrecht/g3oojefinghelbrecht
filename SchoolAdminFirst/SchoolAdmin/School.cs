﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SchoolAdmin
{
    class School
    {
        private string name;

        public string Name
        {
            get { return name; }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    name = value;
                }
            }
        }

        public string Street { get; set; }
        public string PostalCode { get; set; }
        public string City { get; set; }
        public int Id { get; set; }
        public static List<School> List { get; set; }

        public static string ShowAll()
        {
            string text = "Lijst van scholen:\n";
            foreach (var school in List)
            {
                text += $"{school.Name}, {school.Street}, {school.City}, {school.Id}\n";
            }
            return text;
        }

        public string ShowOne()
        {
            string text = "Gegevens van de school {this.Name}, {this.Street}, {this.City}, {this.Id}";
            return text;
        }

    }
}
