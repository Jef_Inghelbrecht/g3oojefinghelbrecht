﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOMonitoraat.Static
{
    class Auto
    {
        public static double snelheid;
        public static double kilometerTeller;

        public static void GasGeven()
        {
            snelheid += 10;
            kilometerTeller += 60;
        }
        public static void Afremmen()
        {
            snelheid -= 10;
            kilometerTeller -= 10 / 60;
        }

        public static string ToPrint()
        {
            return $"Snelheid: {snelheid}\nKilometerTeller: {kilometerTeller}\n";
        }
    }
}
