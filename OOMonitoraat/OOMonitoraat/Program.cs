﻿using System;

namespace OOMonitoraat
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello Monitoraat!");
            Console.WriteLine("1. Auto's met static");
            Console.WriteLine("2. Auto's zonder static");
            char input = Console.ReadKey().KeyChar;
            switch (input)
            {
                case '1':
                    OOMonitoraat.Static.Auto.kilometerTeller = 0;
                    Static.Auto.snelheid = 0;
                    Console.WriteLine(Static.Auto.ToPrint());
                    Static.Auto.GasGeven();
                    Console.WriteLine(Static.Auto.ToPrint());
                    Static.Auto.Afremmen();
                    // probleem: met static heb ik maar 1 auto

                    break;
                case '2':
                    Object.Auto auto1 = new Object.Auto();
                    Object.Auto auto2 = new Object.Auto();
                    
                   auto1.GasGeven();
                    auto1.GasGeven();
                    auto2.GasGeven();
                    auto2.Afremmen();
                    Console.WriteLine($"Auto 1:\n{auto1.ToPrint()}");
                    Console.WriteLine($"Auto 2:\n{auto2.ToPrint()}");
                    // probleem: met static heb ik maar 1 auto
                    break;
            }
        }
    }
}
