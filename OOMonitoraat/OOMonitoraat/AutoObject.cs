﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOMonitoraat.Object
{
    class Auto
    {
        private double snelheid;
        private double kilometerTeller;

        public double Snelheid
        {
            set
            {
                if (this.snelheid <= 120)
                {
                    this.snelheid = value;
                }
            }
            get
            {
                return this.snelheid;
            }
        }


        public Auto()
        {
            snelheid = 0;
            kilometerTeller = 0;
        }
        public void GasGeven()
        {
            Snelheid += 10;
            kilometerTeller += 60;
        }
        public void Afremmen()
        {
            Snelheid -= 10;
            kilometerTeller -= 60;
        }

        public string ToPrint()
        {
            return $"Snelheid: {snelheid}\nKilometerTeller: {kilometerTeller}\n";
        }
    }
}
