﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SchoolAdmin
{
    abstract class Person
    {

        public string FirstName { get; set; }

        private string lastName;

        public string LastName
        {
            get { return lastName; }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    lastName = value;
                }
            }
        }

        protected DateTime BirthDay { get; set; }

        public int Id { get; set; }

        public int SchoolId { get; set; }

        public Person(string firstName, string lastName, DateTime birthDay, int id, int schoolId)
        {
            this.FirstName = firstName;
            this.LastName = lastName;
            this.BirthDay = birthDay;
            this.Id = id;
            this.SchoolId = schoolId;
        }

        public abstract string ShowOne();

        public virtual string GetNameTagText()
        {
            return $"{this.FirstName} {this.LastName}";
        }

        public string GetPlaceholderText()
        {
            return $"{this.FirstName}, {this.LastName}, {this.BirthDay}, {this.Id}, {this.SchoolId}";
        }
    }
}
