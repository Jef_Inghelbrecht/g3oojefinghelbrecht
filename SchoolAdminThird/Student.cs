﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SchoolAdmin
{
    class Student : Person
    {

        public static List<Student> List { get; set; }

        public Student(string firstName, string lastName, DateTime birthDay, int id, int schoolId) : base(firstName, lastName, birthDay, id, schoolId)
        {
        }
        public static string ShowAll()
        {
            string text = "Lijst van studenten:\n";
            foreach (var student in Student.List)
            {
                text+=$"{student.FirstName}, {student.LastName}, {student.BirthDay}, {student.Id}, {student.SchoolId}";
            }
            return text;
        }

        public override string ShowOne()
        {
            return $"Gegevens van de student: {this.FirstName}, {this.LastName}, {this.BirthDay}, {this.Id}, {this.SchoolId}";
        }
    }
}
