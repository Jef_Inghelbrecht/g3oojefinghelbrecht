﻿using System;
using System.Collections.Generic;

namespace SchoolAdmin
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Schoolbeheer");
            var school1 = new School("GO! BS de Spits", "Thonetlaan 106", "2050", "Antwerpen", 1);
            var school2 = new School("GO! Koninklijk Atheneum Deurne", "Fr. Craeybeckxlaan 22", "2100", "Deurne", 2);
            School.List = new List<School>();
            School.List.Add(school1);
            School.List.Add(school2);
            Console.WriteLine(School.ShowAll());
            var student1 = new Student("Mohamed", "El Farisi", new DateTime(1987, 12, 06), 1, 1);
            var student2 = new Student("Sarah", "Jansens", new DateTime(1991, 10, 21), 2, 2);
            var student3 = new Student("Bart", "Jansens", new DateTime(1991, 10, 21),3,2);
            var student4 = new Student("Farah", "El Farisi", new DateTime(1987, 12, 06),4,1);
            Student.List = new List<Student>();
            Student.List.Add(student1);
            Student.List.Add(student2);
            Student.List.Add(student3);
            Student.List.Add(student4);
            Console.WriteLine(Student.ShowAll());

            var lecturer1 = new Lecturer("Adem", "Kaya", new DateTime(1976, 12, 01), 1, 1);
            Console.WriteLine(lecturer1.ShowTaughtCourses());
            var lecturer2 = new Lecturer("Anne", "Wouters", new DateTime(1968, 04, 03), 2, 2);
            Lecturer.List = new List<Lecturer>();
            Lecturer.List.Add(lecturer1);
            Lecturer.List.Add(lecturer2);
            Console.WriteLine(Lecturer.ShowAll());
            Console.WriteLine(lecturer1.ShowOne());

            AdministrativeStaff.List = new List<AdministrativeStaff>();
            var administrativeStaff1 = new AdministrativeStaff("Raul","Jacob", new DateTime(1985, 11, 1),1,1);
            AdministrativeStaff.List.Add(administrativeStaff1);
            Console.WriteLine(AdministrativeStaff.ShowAll());
            Console.WriteLine(administrativeStaff1.ShowOne());

            Console.WriteLine("Lijst met personen:");
            var persons = new List<Person>();
            persons.Add(lecturer1);
            persons.Add(student1);
            persons.Add(administrativeStaff1);
            foreach(var person in persons)
            {
                Console.WriteLine(person.GetNameTagText());
            }

            Course theoryCourse = new TheoryCourse("OO Programmeren", 3);
            Course seminar = new Seminar("Docker");
            lecturer1.Courses.Add(theoryCourse);
            lecturer1.Courses.Add(seminar);
            Console.WriteLine($"Cursussen van Adem: {lecturer1.ShowTaughtCourses()}");

            Console.ReadLine();
        }
    }
}
