﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SchoolAdmin
{
    class Lecturer : Person
    {

        public static List<Lecturer> List { get; set; }

        public List<Course> Courses { get; set; }

        public Lecturer(string firstName, string lastName, DateTime birthDay, int id, int schoolId) : base(firstName, lastName, birthDay, id, schoolId)
        {
            this.Courses = new List<Course>();
        }

        public static string ShowAll()
        {
            string text = "Lijst van lectoren:\n";
            foreach (var lecturer in Lecturer.List)
            {
                text += $"{lecturer.FirstName}, {lecturer.LastName}, {lecturer.BirthDay}, {lecturer.Id}, {lecturer.SchoolId}";
            }
            return text;
        }

        public override string ShowOne()
        {
            return $"Gegevens van de lector: {this.FirstName}, {this.LastName}, {this.BirthDay}, {this.Id}, {this.SchoolId}";
        }

        public string ShowTaughtCourses()
        {
            string result = "Vakken van deze lector:";
            foreach (var course in this.Courses)
            {
                result += $"{course.Title} ({course.CalculateWorkload()})\n";
            }
            return result;
        }
        public override string GetNameTagText()
        {
            return $"(LECTOR) {this.FirstName} {this.LastName}";
        }
    }
}
