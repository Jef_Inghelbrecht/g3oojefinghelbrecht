﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SchoolAdmin
{
    class AdministrativeStaff : Person
    {

        public static List<AdministrativeStaff> List { get; set; }

        public AdministrativeStaff(string firstName, string lastName, DateTime birthDay, int id, int schoolId) : base(firstName, lastName, birthDay, id, schoolId)
        {
        }

        public override string ShowOne()
        {
            return $"Gegevens van administratief personeel: {this.FirstName}, {this.LastName}, {this.Id}, {this.SchoolId}";
        }

        public static string ShowAll()
        {
            string text = "Lijst van administratief personeel:\n";
            foreach (var staff in AdministrativeStaff.List)
            {
                text += $"{staff.FirstName}, {staff.LastName}, {staff.Id}, {staff.SchoolId}";
            }
            return text;
        }

        public override string GetNameTagText()
        {
            return $"(ADMINISTRATIE) {this.FirstName} {this.LastName}";
        }
    }
}
