﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class LerenWerkenMetArraysVanObjecten
    {
        public static void ArraysVanObjecten()
        {
            // een array van objecten declaren
            Student[] mijnKlas = new Student[20];
            Student student1 = new Student();
            Student student2 = new Student();
            mijnKlas[0] = student1;
            mijnKlas[19] = student2;

            Student[] jouwKlas = new Student[]
            {
                new Student(),
                new Student(),
                student1,
                new Student(),
                student2
            };

            mijnKlas[0].Name = "Sarah";
            mijnKlas[19].Name = "Frans";
            jouwKlas[0].Name = "Mohamed";
            jouwKlas[2].Name = "An";

            Student.StudentArray = mijnKlas;
            Console.WriteLine(Student.ShowStudentArray());
            Student.StudentArray = jouwKlas;
            Console.WriteLine(Student.ShowStudentArray());

            List<Student> mijnStudentList = new List<Student>();
            mijnStudentList.Add(student1);
            mijnStudentList.Add(student2);
            Student.StudentList = mijnStudentList;
            Console.WriteLine(Student.ShowStudentList());

            if (mijnStudentList.Contains(student1))
            {
                Console.WriteLine("An zit in de lijst");
            }
            else
            {
                Console.WriteLine("An zit niet in de lijst");

            }
            mijnStudentList.Add(new Student { Name = "Farah" });
            Console.WriteLine(Student.ShowStudentList());

        }

        public static void AskForPrices()
        {
            double[] myArray = new double[20];
            double number;
            double counter = 0;
            double gemiddelde = counter / 20;
            Console.WriteLine("Voer 5 getallen in: ");
            for (int i = 0; i < 5; i++)
            {
                number = Convert.ToDouble(Console.ReadLine());
                counter += number;
                myArray[i] = number;

            }
            foreach (var item in myArray)
            {
                if (item > 5)
                {
                    counter += item;
                    Console.WriteLine(item);
                }
            }
            Console.WriteLine($"Het gemiddelde bedrag is {counter/5}");
        }
    }
}
