﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace OOP
{
    class LerenWerkenMetDateTime
    {
        public static void DayOfWeek()
        {
            Console.Write("Welke dag?");
            int day = int.Parse(Console.ReadLine());
            Console.Write("Welke maand?");
            int month = int.Parse(Console.ReadLine());
            Console.Write("Welk jaar?");
            int year = int.Parse(Console.ReadLine());
            DateTime dateTime = new DateTime(year, month, day);
            CultureInfo belgianCI = new CultureInfo("nl-BE");
            Console.WriteLine($"{dateTime.ToString("dd MMMM yyyy", belgianCI)} is een {dateTime.ToString("dddd", belgianCI)}");
        }

        public static void LeapYear()
        {
            int numberOfLeapYears = 0;
            for (int i = 1800; i <= 2020; i++)
            {
                if (DateTime.IsLeapYear(i))
                {
                    numberOfLeapYears++;
                }
            }
            Console.WriteLine($"Er zijn {numberOfLeapYears} schrikkeljaren tussen 1800 en 2020");
        }

        public static void ArrayTimerProgram ()
        {
            const int totalNumber = 8000000;
            int[] miljoen = new int[totalNumber];
            DateTime before = DateTime.Now;
            for (int i = 0; i < totalNumber; i++)
            {
                miljoen[i] = i;
            }
            DateTime after = DateTime.Now;
            TimeSpan elapsedTime = after - before;
            Console.WriteLine($"Het duurt {elapsedTime.TotalMilliseconds}                milliseconden om een array van een miljoen elementen aan te maken en op te vullen met opeenvolgende waarden.");
        }

        public static void Ticks2000Program(int start = 2000)
        {
            // const int start = 2019;
            DateTime startDate = new DateTime(start, 1, 1);
            DateTime currentDate = DateTime.Now;
            long elapsedTicks = currentDate.Ticks - startDate.Ticks;
            Console.WriteLine($"Sinds 1 januari {start} zijn er {elapsedTicks.ToString()} ticks voorbijgegaan.");
        }
    }
}
