﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Net;

namespace OOP
{
    class SpelenMetStrings
    {
        public static void VerbatimCharacter()
        {
            string text = "Mijn naam is Jef Inghelbrecht\n\tMijn functie is Docent\n\tAP Hogeschool";
            Console.WriteLine(text);
            string textVerbatim = @"Mijn naam is Jef Inghelbrecht
    Mijn functie is Docent
    AP Hogeschool";
            Console.WriteLine(textVerbatim);
        }

        public static void StringsSplitsen()
        {
            string names = "Johan,Farah,Sanne";
            string[] namesSplitted = names.Split(',');
            for (int i = 0; i < namesSplitted.Length; i++)
            {
                Console.WriteLine(namesSplitted[i]);
            }
        }

        public static void StringsSamenvoegen()
        {
            string[] names = { "Johan", "Farah", "Sanne" };
            string text = String.Join(";", names);
            Console.WriteLine(text);
        }

        public static void AndereNuttigeMehoden()
        {
            String text = "Dit schrijfblokje is gemaakt van ecologisch papier";
            Console.WriteLine($"Lengte: {text.Length}");
            Console.WriteLine($"Begin van gemaakt: {text.IndexOf("gemaakt")}");
            Console.WriteLine(text.ToUpper());
            Console.WriteLine(text.Replace("gemaakt", "geproduceerd"));
        }

        public static void CSVUitlezen()
        {
            string[] lines = File.ReadAllLines("soccer.csv");
            for (int i = 0; i < lines.Length; i++)
            {
                string[] splitted = lines[i].Split(';');
                Console.WriteLine($"Voornaam speler: {i} = {splitted[1]}");
                Console.WriteLine($"Familienaam speler: {i}= {splitted[0]}");
                Console.WriteLine($"Geboortejaar speler: {i}= {splitted[2]}");
            }
        }

        public static void CSVDownloaden()
        {
            WebClient wc = new WebClient();
            string csv = wc.DownloadString("http://samplecsvs.s3.amazonaws.com/SalesJan2009.csv");
            string[] split = csv.Split('\r');
            Console.WriteLine(split.Length);
            for (int i = 0; i < split.Length; i++)
            {
                string[] lijnSplit = split[i].Split(',');
                Console.WriteLine($"Data 1: {lijnSplit[0]}");
                Console.WriteLine($"Data 2: {lijnSplit[1]}");
            }
        }
    }
}
