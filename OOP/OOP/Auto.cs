﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class Auto
    {
        public double KilometerTeller { get; set; }
        public double Snelheid { get; set; }

        public Auto()
        {
            KilometerTeller = 0;
            Snelheid = 0;
        }
        public double Remmen()
        {
            return 0;
        }

        public void GasGeven()
        {
            Snelheid += 10;
            KilometerTeller += (Snelheid / 3600 * 100);
        }
    }
}
