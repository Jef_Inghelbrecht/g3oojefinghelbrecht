﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    public enum PokeSpecies { Bulbasaur, Charmander, Squirtle, Pikachu };
    public enum PokeTypes { Grass, Fire, Water, Electric };
    class Pokemon
    {
        private static int totalAttacks;
        public static int TotalAttacks { get { return totalAttacks; } }
        private static int grass = 0;
        public static int Grass { get { return grass; } }
        private static int fire = 0;
        public static int Fire { get { return fire; } }
        private static int water = 0;
        public static int Water { get { return water; } }
        private static int electric = 0;
        public static int Electric { get { return electric; } }

        private int maxHP;

        public int MaxHP
        {
            get { return maxHP; }
            set
            {
                if (value <= 1000)
                {
                    if (value >= 20)
                    {
                        maxHP = value;
                    }
                    else
                    {
                        maxHP = 20;
                    }
                }
                else
                {
                    maxHP = 1000;
                }
            }
        }

        private int hp;

        public int HP
        {
            get { return hp; }
            set
            {
                if (value <= 0)
                {
                    hp = 0;
                }
                else if (value >= MaxHP)
                {
                    hp = MaxHP;
                }
                else
                {
                    hp = value;
                }
            }
        }

        public PokeSpecies PokeSpecie { get; set; }
        public PokeTypes PokeType { get; set; }

        public Pokemon()
        {

        }

        public Pokemon(int maxHP, int hp,
             PokeSpecies pokeSpecie,
             PokeTypes pokeType)
        {
            MaxHP = maxHP;
            HP = hp;
            PokeSpecie = pokeSpecie;
            PokeType = pokeType;
        }

        public Pokemon(int maxHP,
             PokeSpecies pokeSpecie,
             PokeTypes pokeType) : this(maxHP, maxHP/2,
                 pokeSpecie, pokeType)
        {
            // HP = maxHP / 2;
        }

        public void Attack()
        {
            totalAttacks++;
            if (PokeType == PokeTypes.Grass)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                grass++;
            }
            else if (PokeType == PokeTypes.Fire)
            {
                fire++;
                Console.ForegroundColor = ConsoleColor.Red;
            }
            else if (PokeType == PokeTypes.Water)
            {
                water++;
                Console.ForegroundColor = ConsoleColor.Blue;
            }
            else if (PokeType == PokeTypes.Electric)
            {
                electric++;
                Console.ForegroundColor = ConsoleColor.Yellow;
            }
            Console.WriteLine($"{PokeSpecie.ToString().ToUpper()}!");
            Console.ResetColor();
        }
        public static Pokemon FirstConsciousPokemon(Pokemon[] pokemons)
        {
            for (int i = 0; i < pokemons.Length; i++)
            {
                if (pokemons[i].HP >= 1)
                {
                    return pokemons[i];
                }
            }
            return null;
        }
    }
}
