﻿using System;
using Wiskunde.Meetkunde;
using OOP;

namespace LerenWerkenMetOOP
{
    class Program
    {
        static void Main(string[] args)
        {
            bool done = false;
            while (!done)
            {
                Console.WriteLine("Hoofdmenu");
                Console.WriteLine("1: Beginnen met OO");
                Console.WriteLine("2: DateTime: leren werken met objecten");
                Console.WriteLine("3: De Studentklasse testen");
                Console.WriteLine("4: Pokemon oefeningen");
                Console.WriteLine("5: Spelen met strings");
                Console.WriteLine("6: Arrays en klassen");
                Console.WriteLine("Q. Het is genoeg voor vandaag!");
                char response = Console.ReadKey().KeyChar;
                Console.WriteLine();
                switch (response)
                {
                    case '1':
                        SubMenuBeginnenMetOO();
                        break;
                    case '2':
                        SubMenuDateTime();
                        break;
                    case '3':
                        TestStudent();
                        break;
                    case '4':
                        SubMenuPokemon();
                        break;
                    case '5':
                        SubMenuSpelenMetStrings();
                        break;
                    case '6':
                        SubMenuArraysEnKlassen();
                        break;
                    case 'Q':
                        Console.WriteLine("Ga je ermee stoppen? J/N");
                        if (Console.ReadKey().KeyChar == 'J')
                        {
                            done = true;
                        }
                        break;
                }
                Console.Clear();
            }
            Console.ReadKey();
        }
        static void TestStudent()
        {
            OOP.Student student1 = new Student();
            student1.ClassGroup = ClassGroups.EA2;
            student1.Age = 21;
            student1.Name = "Bob Dylan";
            student1.MarkCommunication = 18;
            student1.MarkProgrammingPrinciples = 19;
            student1.MarkWebTech = 20;
            string text = student1.ShowOverview();
            Console.WriteLine(text);

            Student student2 = new Student();
            student2.ClassGroup = ClassGroups.EB1;
            student2.Age = 130;
            student2.Name = "Sareh El Farisi";
            student2.MarkCommunication = 19;
            student2.MarkProgrammingPrinciples = 19;
            student2.MarkWebTech = 20;
            text = student2.ShowOverview();
            Console.WriteLine(text);

            Console.ReadKey();
        }
        static void SubMenuBeginnenMetOO()
        {
            bool done = false;
            while (!done)
            {
                Console.WriteLine("Submenu Beginnen met OO");
                Console.WriteLine("1. Wat is static?");
                Console.WriteLine("2. Public vs Private");
                Console.WriteLine("3. Teken een lijn");
                Console.WriteLine("4. Teken een lijn met een *");
                Console.WriteLine("5. Teken een lijn met 20 *en");
                Console.WriteLine("6. Teken een lijn met de standaard static teken waarde");
                Console.WriteLine("7. Teken een lijn met de static teken waarde = +");
                Console.WriteLine("8. Teken een rechthoek 10 tekens lang en 3 lijnen hoog");
                Console.WriteLine("9. Teken een volle rechthoek en geef hoogte en breedte op");
                Console.WriteLine("a. Teken een lege rechthoek en geef hoogte en breedte op");
                Console.WriteLine("Q. Terug naar het hoofdmenu");
                char response = Console.ReadKey().KeyChar;
                switch (response)
                {
                    case '1':
                        WatIsStatic();
                        break;
                    case '2':
                        PrivateVsPublic();
                        break;
                    case '3':
                        /* ik kan de methode Lijn oproepen
                         * omdat die static is en ik hoef
                         * dus niet eerst een object of instantie
                         * te maken */
                        Console.WriteLine(Vormen.Lijn());
                        break;
                    case '4':
                        Console.WriteLine(Vormen.Lijn('*'));
                        break;
                    case '5':
                        Console.WriteLine(Vormen.Lijn('*', 20));
                        break;
                    case '6':
                        Console.WriteLine(Vormen.LijnDieHetVeldMetDeNaamTekenGebruikt());
                        break;
                    case '7':
                        Vormen.teken = '+';
                        Console.WriteLine(Vormen.LijnDieHetVeldMetDeNaamTekenGebruikt());
                        break;
                    case '8':
                        Console.WriteLine(Vormen.VolleRechthoek());
                        break;
                    case '9':
                        Console.WriteLine(Vormen.VolleRechthoek(20, 20));
                        break;
                    case 'a':
                        Console.WriteLine(Vormen.LegeRechthoek(20, 20));
                        break;

                    case 'Q':
                        done = true;
                        break;
                }
            }
        }


        static void SubMenuDateTime()
        {
            bool done = false;
            while (!done)
            {
                Console.WriteLine("Submenu DateTime: leren werken met objecten");
                Console.WriteLine("1: H8 Dag van de week");
                Console.WriteLine("2: H8 Schrikkeljaar teller");
                Console.WriteLine("3: Oefening: H8-simpele-timing");
                Console.WriteLine("4:Oefening: H8-ticks-sinds-2000");
                Console.WriteLine("Q. Terug naar hoofdmenu");
                char response = Console.ReadKey().KeyChar;
                switch (response)
                {
                    case '1':
                        OOP.LerenWerkenMetDateTime.DayOfWeek();
                        break;
                    case '2':
                        OOP.LerenWerkenMetDateTime.LeapYear();
                        break;
                    case '3':
                        OOP.LerenWerkenMetDateTime.ArrayTimerProgram();
                        break;
                    case '4':
                        OOP.LerenWerkenMetDateTime.Ticks2000Program(2005);
                        break;
                    case 'Q':
                        done = true;
                        break;
                }
            }
        }
        static void WatIsStatic()
        {
            Console.WriteLine("Leren werken met DGP 'ding gericht programmeren!");
            // instantie of exemplaar van de klasse maken
            // Wiskunde.Meetkunde.Vormen vormen = new Wiskunde.Meetkunde.Vormen();
            // de methode van de instantie of het object gebruiken
            // Console.WriteLine(vormen.Lijn());
            // als de methode lijn static is, behoort die tot
            // de klasse en niet tot het object
            Console.WriteLine(Wiskunde.Meetkunde.Vormen.Lijn());
        }

        static void PrivateVsPublic()
        {
            // het veld kleur van de klasse Vormen is hier
            // zichtbaar omdat het public is
            Wiskunde.Meetkunde.Vormen.kleur = ConsoleColor.Red;
            Wiskunde.Meetkunde.Vormen vormen = new Wiskunde.Meetkunde.Vormen();
            // het object, instantie of exemplaar geeft geen toegang 
            // tot static methode
            // vormen.LijnInKleuren();
            Wiskunde.Meetkunde.Vormen.Achtergrond = ConsoleColor.Cyan;
            Wiskunde.Meetkunde.Vormen.LijnInKleur();
            // welke kleur wordt door Vormen gebruikt?
            if (Wiskunde.Meetkunde.Vormen.Achtergrond == ConsoleColor.Red)
            {
                Console.WriteLine("De achtergrondkleur is rood.");
            }
            else
            {
                Console.WriteLine("De achtergrondkleur is niet rood.");

            }
        }

        static void SubMenuSpelenMetStrings()
        {
            bool done = false;
            while (!done)
            {
                Console.WriteLine("1: Verbatim character");
                Console.WriteLine("2: Split");
                Console.WriteLine("3: Join");
                Console.WriteLine("4: Andere nuttige methoden");
                Console.WriteLine("5: Lokaal CSV bestand inlezen");
                Console.WriteLine("6: CSV bestand downloaden en inlezen");
                Console.WriteLine("Q. Terug naar hoofdmenu");
                char response = Console.ReadKey().KeyChar;
                Console.WriteLine();
                switch (response)
                {
                    case '1':
                        SpelenMetStrings.VerbatimCharacter();
                        break;
                    case '2':
                        SpelenMetStrings.StringsSplitsen();
                        break;
                    case '3':
                        SpelenMetStrings.StringsSamenvoegen();
                        break;
                    case '4':
                        SpelenMetStrings.AndereNuttigeMehoden();
                        break;
                    case '5':
                        SpelenMetStrings.CSVUitlezen();
                        break;
                    case '6':
                        SpelenMetStrings.CSVDownloaden();
                        break;
                    case 'Q':
                        done = true;
                        break;
                }
            }
        }

        static void SubMenuPokemon()
        {
            bool done = false;
            while (!done)
            {
                Console.WriteLine("Submenu Pokemon");
                Console.WriteLine("1: Pokemon attack");
                Console.WriteLine("2: Conscious Pokemon");
                Console.WriteLine("3: Conscious Pokemon (improved)");
                Console.WriteLine("4: Pokemon attack with constructor");
                Console.WriteLine("5: Pokemon constructor chaining");
                Console.WriteLine("6: Pokemon Counter Demonstration");

                Console.WriteLine("Q. Terug naar hoofdmenu");
                char response = Console.ReadKey().KeyChar;
                Console.WriteLine();
                switch (response)
                {
                    case '1':
                        MakePokemon();
                        break;
                    case '2':
                        TestConsciousPokemon();
                        break;
                    case '3':
                        TestConsciousPokemonSafe();
                        break;
                    case '4':
                        MakePokemonWithConstructor();
                        break;
                    case '5':
                        ConstructPokemonChained();
                        break;
                    case '6':
                        DemonstratePokemonCounter();
                        break;
                    case 'Q':
                        done = true;
                        break;
                }
            }
        }
        static void MakePokemon()
        {
            Pokemon bulbasaur = new Pokemon();
            bulbasaur.MaxHP = 20;
            bulbasaur.HP = 20;
            bulbasaur.PokeSpecie = PokeSpecies.Bulbasaur;
            bulbasaur.PokeType = PokeTypes.Grass;
            bulbasaur.Attack();

            Pokemon charmander = new Pokemon();
            charmander.MaxHP = 16;
            charmander.HP = 20;
            charmander.PokeSpecie = PokeSpecies.Charmander;
            charmander.PokeType = PokeTypes.Fire;
            charmander.Attack();
        }

        static void MakePokemonWithConstructor()
        {
            Pokemon bulbasaur = new Pokemon(20, 0,
                PokeSpecies.Bulbasaur, PokeTypes.Grass);
            bulbasaur.Attack();
            Pokemon charmander = new Pokemon(20, 0,
               PokeSpecies.Charmander, PokeTypes.Fire);
            charmander.Attack();
        }

        public static void ConstructPokemonChained()
        {
            Pokemon squirtle = new Pokemon(20, PokeSpecies.Squirtle, PokeTypes.Water);
            Console.WriteLine($"De nieuwe Squirtle heeft maximum {squirtle.MaxHP} HP en heeft momenteel {squirtle.HP} HP.");
        }

        public static void DemonstratePokemonCounter()
        {
            Pokemon bulbasaur1 = new Pokemon(20, 0,
               PokeSpecies.Bulbasaur, PokeTypes.Grass);
            Pokemon bulbasaur2 = new Pokemon(20, 0,
              PokeSpecies.Bulbasaur, PokeTypes.Grass);
            Pokemon charmander1 = new Pokemon(20, 0,
              PokeSpecies.Charmander, PokeTypes.Fire);
            Pokemon charmander2 = new Pokemon(20, 0,
            PokeSpecies.Charmander, PokeTypes.Fire);
            // maak zelf nog pokemons bij
            for (int i = 0; i < 10; i++)
            {
                bulbasaur1.Attack();
            }
            for (int i = 0; i < 15; i++)
            {
                bulbasaur2.Attack();
            }
            for (int i = 0; i < 10; i++)
            {
                charmander1.Attack();
            }
            for (int i = 0; i < 25; i++)
            {
                charmander2.Attack();
            }
            Console.WriteLine($"Aantal aanvallen van Pokémon met type Grass: {Pokemon.Grass}");
            Console.WriteLine($"Aantal aanvallen van Pokémon met type Fire: {Pokemon.Fire}");
            Console.WriteLine($"Aantal aanvallen van Pokémon met type Water: {Pokemon.Water}");
            Console.WriteLine($"Aantal aanvallen van Pokémon met type Electric: {Pokemon.Electric}");
            Console.WriteLine($"Totaal aantal aanvallen door alle instanties van Pokemon: {Pokemon.TotalAttacks}");
        }

        public static void TestConsciousPokemon()
        {
            // De 4 pokemon maken en in array stoppen
            //  Bulbasaur en Charmander 0 HP hebben 
            // Squirtle 2 HP en Picachu 20 HP
            Pokemon bulbasaur = new Pokemon();
            bulbasaur.MaxHP = 20;
            bulbasaur.HP = 0;
            bulbasaur.PokeSpecie = PokeSpecies.Bulbasaur;
            bulbasaur.PokeType = PokeTypes.Grass;

            Pokemon charmander = new Pokemon();
            charmander.MaxHP = 20;
            charmander.HP = 3;
            charmander.PokeSpecie = PokeSpecies.Charmander;
            charmander.PokeType = PokeTypes.Fire;

            Pokemon squirtle = new Pokemon();
            squirtle.MaxHP = 20;
            squirtle.HP = 2;
            squirtle.PokeSpecie = PokeSpecies.Squirtle;
            squirtle.PokeType = PokeTypes.Water;

            Pokemon pikachu = new Pokemon();
            pikachu.MaxHP = 20;
            pikachu.HP = 20;
            pikachu.PokeSpecie = PokeSpecies.Pikachu;
            pikachu.PokeType = PokeTypes.Electric;

            // we stoppen de instanties die we gemaakt hebben in een array
            Pokemon[] pokemonArray = { bulbasaur, charmander, squirtle, pikachu };
            // vermits FirstConsciousPokemon een static methode is
            // kunnen we die direct vanuit de klasse oproepen en niet
            // vanuit een object van die klasse
            Pokemon firstPokemon = Pokemon.FirstConsciousPokemon(pokemonArray);
            firstPokemon.Attack();
        }

        public static void TestConsciousPokemonSafe()
        {
            // De 4 pokemon maken en in array stoppen
            Pokemon bulbasaur = new Pokemon();
            bulbasaur.MaxHP = 20;
            bulbasaur.HP = 0;
            bulbasaur.PokeSpecie = PokeSpecies.Bulbasaur;
            bulbasaur.PokeType = PokeTypes.Grass;

            Pokemon charmander = new Pokemon();
            charmander.MaxHP = 20;
            charmander.HP = 0;
            charmander.PokeSpecie = PokeSpecies.Charmander;
            charmander.PokeType = PokeTypes.Fire;

            Pokemon squirtle = new Pokemon();
            squirtle.MaxHP = 20;
            squirtle.HP = 0;
            squirtle.PokeSpecie = PokeSpecies.Squirtle;
            squirtle.PokeType = PokeTypes.Water;

            Pokemon pikachu = new Pokemon();
            pikachu.MaxHP = 20;
            pikachu.HP = 0;
            pikachu.PokeSpecie = PokeSpecies.Pikachu;
            pikachu.PokeType = PokeTypes.Electric;

            Pokemon[] pokemonArray = { bulbasaur, charmander, squirtle, pikachu };
            Pokemon firstPokemon = Pokemon.FirstConsciousPokemon(pokemonArray);
            if (firstPokemon != null)
            {
                firstPokemon.Attack();
            }
            else
            {
                Console.WriteLine("Al je Pokémon zijn KO! Haast je naar het Pokémon center.");
            }
        }

        static void SubMenuArraysEnKlassen()
        {
            bool done = false;
            while (!done)
            {
                Console.WriteLine("1: Array van objecten");
                Console.WriteLine("2: Ask for prices");
                Console.WriteLine("Q: terug naar hoofdmenu");
                char response = Console.ReadKey().KeyChar;
                Console.WriteLine();
                switch (response)
                {
                    case '1':
                        LerenWerkenMetArraysVanObjecten.
                            ArraysVanObjecten();
                        break;
                    case '2':
                        LerenWerkenMetArraysVanObjecten.
                            AskForPrices();
                        break;
                    case '3':
                        break;
                    case 'Q':
                        done = true;
                        break;
                }
            }
        }

    }
}
