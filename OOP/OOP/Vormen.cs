﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wiskunde.Meetkunde
{
    class Vormen
    {
        // voor elk kenmerk van het ding vorm 
        // declaren we een veld
        public static ConsoleColor kleur;
        // default teken is -
        public static char teken = '-';
        // de backgroundcolor moet rood, oranje of groen zijn
        // dus moet die eerst gecheckt worden vooraleer
        // toegekend te worden
        // dus maken we die private
        private static ConsoleColor achtergrond;
        // dat veld is in Main (nl. buiten de klasse) niet zichtbaar
        // ik het een property om het te wijzigen
        public static ConsoleColor Achtergrond
        {
            set {
                if (value == ConsoleColor.Red || value == ConsoleColor.Green
                    || value == ConsoleColor.Yellow)
                {
                    achtergrond = value;
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Alleen rood, geel en groen!");
                    Console.ResetColor();
                    achtergrond = ConsoleColor.Black;
                }
            }
            get { return achtergrond; }
        }

        // deze methode is identiek voor alle mogelijke
        // instanties (exemplaren) van deze Vormenklassen
        public static string Lijn()
        {
            return new string('-', 10);
        }

        // overloading: zelfde naam met andere parameters
        public static string Lijn(char teken)
        {
            return new string(teken, 10);
        }

        public static string Lijn(char teken, int aantal)
        {
            return new string(teken, aantal);
        }

        public static string LijnDieHetVeldMetDeNaamTekenGebruikt()
        {
            // ik wil het teken van de klasse gebruiken
            return new string(teken, 10);
        }
        // die methode is static omdat
        // ze voor alle instanties of exemplaren
        // identiek is.
        public static void LijnInKleur()
        {
            Console.ForegroundColor = kleur;
            Console.BackgroundColor = achtergrond;
            Console.WriteLine(Lijn());
            Console.ResetColor();
        }

        public static string VolleRechthoek()
        {
            string lijn = new string(teken, 10);
            return $"{lijn}\n{lijn}\n{lijn}\n";
        }

        public static string VolleRechthoek(byte hoogte, byte breedte)
        {
            string lijn = new string(teken, breedte);
            string rechthoek = "";
            for (byte i = 1; i <= hoogte; i++)
            {
                rechthoek += $"{lijn}\n";
            }
            return rechthoek;
        }

        public static string LegeRechthoek(byte hoogte, byte breedte)
        {
            string lijn = new string(teken, breedte);
            string openLijn = new string(' ', breedte - 2);
            string rechthoek = $"{lijn}\n";
            for (byte i = 1; i <= hoogte - 2; i++)
            {
                rechthoek += $"{teken}{openLijn}{teken}\n";
            }
            rechthoek += $"{lijn}\n";


            return rechthoek;
        }
    }
}
